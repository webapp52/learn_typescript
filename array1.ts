const names: string[] = [];
names.push("Dylan");
names.push("Eiei");
console.log(names[0]);
console.log(names[1]);
console.log(names.length);
//names.push(3);

for(let i = 0 ; i<names.length ; i++){
    console.log(names[i]);
}

console.log("----for in----");

for(let i in names){
    console.log(names[i]);
}


console.log("----for each function----");


names.forEach(function(name){
   console.log(name); 
});
