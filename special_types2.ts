let w: unknown = 1;
w = "string";
w = {
    runANonExistentMethod: () => {
        console.log("Who am I ?");
    }
} as {runANonExistentMethod: () => void}

if(typeof w == 'object' && w !== null){
    (w as {runANonExistentMethod: () => void}).runANonExistentMethod();
}